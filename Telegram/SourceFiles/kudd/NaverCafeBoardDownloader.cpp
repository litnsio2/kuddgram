#include "stdafx.h"
#include "NaverCafeBoardDownloader.h"
#include "KuddModuleManager.h"
#include "KuddDigalFlashParser.h"
#include "KuddDigalFlashTypes.h"
#include <QtNetwork/QNetworkRequest>
#include <QtNetwork/QNetworkReply>

// example : http://qt-project.org/doc/qt-4.8/network-http-httpwindow-cpp.html
// threads?  http://comments.gmane.org/gmane.comp.lib.qt.general/31345
// you're doing it wrong : http://blog.qt.digia.com/blog/2010/06/17/youre-doing-it-wrong/
// qthread and qobject : http://qt-project.org/doc/qt-5/threads-qobject.html

using namespace kudd;

NaverCafeBoardDownloader::NaverCafeBoardDownloader()
    : reply_(nullptr)
{
}

bool NaverCafeBoardDownloader::download(quint8 boardType, const QString& uri, const QString& cafeName)
{
    currentBoardType_ = boardType;
    cafeName_ = cafeName;

    bool ret = true;

    try {
        reply_ = nam_.get(QNetworkRequest(uri));
        connect(reply_, SIGNAL(finished()), this, SLOT(httpFinished()));
        connect(reply_, SIGNAL(readyRead()), this, SLOT(httpReadyRead()));
        connect(reply_, SIGNAL(downloadProgress(quint64, quint64)), this, SLOT(updateDataReadProgress(quint64, quint64)));
        connect(reply_, SIGNAL(error(QNetworkReply::NetworkError)), this, SLOT(onReplyError(QNetworkReply::NetworkError)));
    }
    catch (...) {
        ret = false;
    }

    return ret;
}

void NaverCafeBoardDownloader::httpFinished()
{
    //KuddNaverCafeArticles articles;
    KuddNaverCafeArticles articles;
    lastResponse_ = reply_->readAll();
    const char* body = lastResponse_.data();

    KuddDigalFlashParser::parseArticles(body, cafeName_, &articles);

    articles.setArticleType(currentBoardType_);

    emit boardDownloaded(currentBoardType_, articles);
}

void NaverCafeBoardDownloader::httpReadyRead()
{
}

void NaverCafeBoardDownloader::updateDataReadProgress(quint64 bytesRead, quint64 totalBytes)
{
}

void NaverCafeBoardDownloader::onReplyError(QNetworkReply::NetworkError)
{
}
