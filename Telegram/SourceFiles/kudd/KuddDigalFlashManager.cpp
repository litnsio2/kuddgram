#include "stdafx.h"
#include <QtCore/QSet>
#include <QtCore/QMap>
#include "KuddModuleManager.h"
#include "KuddDigalFlashManager.h"
#include "NaverCafeBoardDownloader.h"

using namespace kudd;

namespace {
    const QString flashUri("http://m.cafe.naver.com/ArticleList.nhn?search.clubid=10115278&search.menuid=370");
    const QString regularFlashUri("http://m.cafe.naver.com/ArticleList.nhn?search.clubid=10115278&search.menuid=642");
    const QString etcUri("http://m.cafe.naver.com/ArticleList.nhn?search.clubid=10115278&search.menuid=369");
    const QString cafeName("temadica");

    class AutoLocker {
    public:
        AutoLocker(QMutex& mutex)
            : mutex_(mutex)
        {
            mutex_.lock();
        }
        ~AutoLocker()
        {
            mutex_.unlock();
        }
    private:
        QMutex& mutex_;
    };
}

KuddDigalFlashManager::KuddDigalFlashManager(KuddModuleManager* owner)
    : owner_(owner)
    , working_(false)
{
    downloader_.reset(new NaverCafeBoardDownloader());

    connect(downloader_.data(), SIGNAL(boardDownloaded(quint8, const KuddNaverCafeArticles&)), this, SLOT(boardDownloaded(quint8, const KuddNaverCafeArticles&)));
    connect(downloader_.data(), SIGNAL(boardDownloadError(QNetworkReply::NetworkError)), this, SLOT(boardDownloadError(QNetworkReply::NetworkError)));
}

void KuddDigalFlashManager::check()
{
    {
        AutoLocker al(mutex_);
        working_ = true;
    }
    
    currentArticles_.clear();

    downloader_->download(KuddNaverCafeArticle::FLASH, flashUri, cafeName);
}

void KuddDigalFlashManager::boardDownloaded(quint8 boardType, const KuddNaverCafeArticles& articles)
{
    currentArticles_.copyArticles(articles);

    if (boardType == KuddNaverCafeArticle::FLASH) {
        downloader_->download(KuddNaverCafeArticle::REGULAR, regularFlashUri, cafeName);
    }
    else if (boardType == KuddNaverCafeArticle::REGULAR) {
        downloader_->download(KuddNaverCafeArticle::ETC, etcUri, cafeName);
    }
    else {
        checkForNewArticles();

        AutoLocker al(mutex_);
        working_ = false;
    }
}

void KuddDigalFlashManager::checkForNewArticles()
{
    bool commentChanged = false;
    bool articleChanged = false;

    if (lastArticles_.empty()) {
        lastArticles_.swap(currentArticles_);
    }
    else {
        QSet<QString> lastArticleSet, currentArticleSet;
        QMap<QString, KuddNaverCafeArticleSp> lastArticleMap, currentArticleMap;

        for (auto& each : lastArticles_.articles()) {
            lastArticleSet.insert(each->articleId());
            lastArticleMap.insert(each->articleId(), each);
        }
        for (auto& each : currentArticles_.articles()) {
            currentArticleSet.insert(each->articleId());
            currentArticleMap.insert(each->articleId(), each);
        }

        for (auto& each : currentArticleSet) {
            // check title, comment number
            KuddNaverCafeArticleSp curArticle = currentArticleMap[each];

            auto where = lastArticleMap.find(each);
            if (where != lastArticleMap.end()) {
                KuddNaverCafeArticleSp prevArticle = lastArticleMap[each];

                if (prevArticle->title() != curArticle->title()) {
                    curArticle->setIsNew(true);
                    articleChanged = true;
                }
                if (prevArticle->numComments() != curArticle->numComments()) {
                    curArticle->setCommentNumChanged(true);
                    curArticle->setNumPreviousComments(prevArticle->numComments());
                    commentChanged = true;
                }
            }
            else {
                curArticle->setIsNew(true);
                articleChanged = true;
            }
        }

        lastArticles_.swap(currentArticles_);
    }

    //if (commentChanged || articleChanged) {
    if (articleChanged) {
        // FIXME : name
        qDebug() << "number of articles : " << lastArticles_.articles().size();
        owner_->onNewArticleDetected("temadica", lastArticles_);
    }
}

void KuddDigalFlashManager::boardDownloadError(QNetworkReply::NetworkError error)
{
    AutoLocker al(mutex_);
    working_ = false;
}
