#ifndef _KUDD_DIGAL_FLASH_TYPES_H_
#define _KUDD_DIGAL_FLASH_TYPES_H_

#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtCore/QSet>
#include <QtCore/QSharedPointer>
#include <QtCore/QByteArray>
#include "../history.h"

namespace kudd {
    class KuddSequentialMtpRepository;
    class KuddMtpArticleMessageType;
    class KuddMtpDialogMessageType;

    class KuddNaverCafeArticle {
    public:
        enum : quint8 {
            REGULAR = 0, // 정모, 신출공지 
            FLASH,	     // 출사, 번개공지 
            ETC,		 // 기타공지 
        }; // quint8

        KuddNaverCafeArticle();

        QString& title() { return title_; }

        const QString& title() const     { return title_; }
        const QString& date() const      { return date_; }
        const QString& author() const    { return author_; }
        const QString& link() const      { return link_; }
        const QString& articleId() const { return articleId_; }
        bool isNew() const                    { return new_; }
        quint8 type() const                    { return type_; }
        bool commentNumChanged() const        { return commentNumChanged_; }
        quint16 numComments() const            { return numComments_; }
        quint16 numPreviousComments() const    { return numPreviousComments_; }

        void setTitle(const QString& title)   { title_ = title; }
        void setDate(const QString& date)     { date_ = date; }
        void setAuthor(const QString& author) { author_ = author; }
        void setLink(const QString& link)     { link_ = link; }
        void setArticleId(const QString& id)  { articleId_ = id; }
        void setIsNew(bool isNew)                  { new_ = isNew; }
        void setType(quint8 type)                  { type_ = type; }
        void setCommentNumChanged(bool changed)    { commentNumChanged_ = changed; }
        void setNumComments(quint16 num)            { numComments_ = num; }
        void setNumPreviousComments(quint16 num)    { numPreviousComments_ = num; }

        bool operator== (const KuddNaverCafeArticle& r) const;
        bool operator!= (const KuddNaverCafeArticle& r) const;

    private:
        QString title_;
        QString date_;
        QString author_;
        QString link_;
        QString articleId_;
        quint8 type_;
        bool commentNumChanged_;
        quint16 numComments_;
        quint16 numPreviousComments_;
        bool new_;
    };
    typedef QSharedPointer<KuddNaverCafeArticle> KuddNaverCafeArticleSp;

    class KuddNaverCafeArticles : public HistoryItem {
    public:
        KuddNaverCafeArticles();
        KuddNaverCafeArticles(History* history, HistoryBlock* block, MsgId msgId, bool out, bool unread, QDateTime msgDate, int32 from);

    public:
        void clear() { articles_.clear(); articleTitles_.clear(); }
        void setArticleType(quint8 type);
        void swap(KuddNaverCafeArticles& articles) { articles_.swap(articles.articles()); }
        void addArticle(const KuddNaverCafeArticleSp& article) { articles_.push_back(article); }
        void copyArticles(const KuddNaverCafeArticles& articles);

        bool empty() const { return articles_.empty(); }
        const KuddNaverCafeArticleSp& article(qint32 index) const { return articles_.at(index); }
        QVector<KuddNaverCafeArticleSp>& articles() { return articles_; }
        const QVector<KuddNaverCafeArticleSp>& articles() const { return articles_; }

    public: // implementation of HistoryItem
        // as HistoryElem
        virtual void initDimensions(const HistoryItem *parent = 0);
        virtual int32 resize(int32 width, bool dontRecountText = false, const HistoryItem *parent = 0);

        // as HistoryItem
        virtual void draw(QPainter &p, uint32 selection) const;
        virtual void drawInDialog(QPainter &p, const QRect &r, bool act, const HistoryItem *&cacheFor, Text &cache) const;
        virtual QString notificationText() const;

    public:
        void convertToMtpMessages(KuddMtpArticleMessageType* articles, KuddSequentialMtpRepository* repository) const;
        void convertToMtpDialogs(KuddMtpDialogMessageType* dialogs, KuddSequentialMtpRepository* repository) const;

    private:
        QVector<KuddNaverCafeArticleSp> articles_;
        QSet<QString> articleTitles_;

    private:
        void makeHistory();
    };

    class KuddMtpRepositoryWritable {
    public:
        virtual void write(KuddSequentialMtpRepository* repository) = 0;
    };

    class KuddMtpBaseType : KuddMtpRepositoryWritable {
    public:
        KuddMtpBaseType(mtpTypeId typeId) : typeId_(typeId) {}

        void setType(mtpTypeId typeId) {
            typeId_ = typeId;
        }
        operator mtpTypeId();

    public:
        virtual void write(KuddSequentialMtpRepository* repository);

    private:
        mtpTypeId typeId_;
    };

    class KuddMtpArticleType : public KuddMtpBaseType {
    public:
        KuddMtpArticleType();
        KuddMtpArticleType(const KuddNaverCafeArticle& article);

        /*
                MTPDmessage &v(_message());
                v.vflags.read(from, end);
                v.vid.read(from, end);
                v.vfrom_id.read(from, end);
                v.vto_id.read(from, end);
                v.vdate.read(from, end);
                v.vmessage.read(from, end);
                v.vmedia.read(from, end);
                MTPint vflags;
                MTPint vid;
                MTPint vfrom_id;
                MTPPeer vto_id;
                MTPint vdate;
                MTPstring vmessage;
                MTPMessageMedia vmedia;
                */

    public:
        virtual void write(KuddSequentialMtpRepository* repository);

        qint32 vId() const { return vid_.v; }

    private:
        MTPint vflags_;
        MTPint vid_;
        MTPint vfrom_id_;
        MTPint vto_id_;     // read mtp type is MTPPeer.. but we need only id here.
        MTPint vdate_;

        QString vmessage_;
        MTPMessageMedia vmedia_;
    };

    class KuddMtpDummyType : public KuddMtpBaseType {
    public:
        KuddMtpDummyType(mtpTypeId type)
            : KuddMtpBaseType(type)
        {}
    };

    // chat room information.
    class KuddMtpChatType : public KuddMtpBaseType {
    public:
        KuddMtpChatType();
        KuddMtpChatType(const KuddNaverCafeArticle& article);

    public:
        void write(KuddSequentialMtpRepository* repository);

    private:
        MTPint vid_;
        QString vtitle_;
        MTPChatPhoto vphoto_;
        MTPint vparticipants_count_;
        MTPint vdate_;
        bool vleft_;
        MTPint vversion_;
    };

    class KuddMtpChatsType : public KuddMtpBaseType {
    public:
        KuddMtpChatsType();

    public:
        void write(KuddSequentialMtpRepository* repository);
        void setChats(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository);

    private:
        quint32 count_;
        QVector<KuddMtpChatType> chats_;
    };

    class KuddMtpUserType : public KuddMtpBaseType {
    public:
        KuddMtpUserType();

    public:
        void write(KuddSequentialMtpRepository* repository);

    private:
    };

    class KuddMtpUsersType : public KuddMtpBaseType {
    public:
        KuddMtpUsersType();

    public:
        void write(KuddSequentialMtpRepository* repository);

    private:
        quint32 count_;
    };

    class KuddMtpArticlesType : public KuddMtpBaseType {
    public:
        KuddMtpArticlesType();
        ~KuddMtpArticlesType();

        void setArticles(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository);
        qint32 lastArticleId() const;

    public:
        void write(KuddSequentialMtpRepository* repository);

    private:
        quint32 count_;
        QVector<KuddMtpArticleType> articles_;
    };

    class KuddMtpArticleMessageType : public KuddMtpBaseType {
    public:
        KuddMtpArticleMessageType();

        KuddMtpArticlesType& articles() { return  articles_; }
        const KuddMtpArticlesType& articles() const { return articles_; }

        void convert(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository);

    public:
        virtual void write(KuddSequentialMtpRepository* repository);

    private: // KuddMtpArticlesType
        KuddMtpArticlesType articles_;
        KuddMtpChatsType chats_;
        KuddMtpUsersType users_;
    };

    class KuddMtpPeerNotifySettings : public KuddMtpBaseType {
    public:
        KuddMtpPeerNotifySettings();

    public:
        virtual void write(KuddSequentialMtpRepository* repository);

    private:
        MTPInt vmute_until_;
        QString vsound_;
        bool vshow_previews_;
        MTPInt vevent_mask_;
    };

    class KuddMtpDialogType : public KuddMtpBaseType {
    public:
        KuddMtpDialogType();

    public:
        virtual void write(KuddSequentialMtpRepository* repository);

    public:
        void setTopMessage(qint32 topMessage) { vtop_message_.v = topMessage; }
        void setUnreadCount(qint32 unreadCount) { vtop_unread_count_.v = unreadCount; }

    private:
        // MTPpeer
        qint32 vpeerType_;    // mtpc_peerUser or mtpc_peerChat
        MTPInt vpeer_;       // peerUser or peerChat
        // ..MTPpeer

        MTPInt vtop_message_;
        MTPInt vtop_unread_count_;

        KuddMtpPeerNotifySettings vnotify_settings_;    // not serialized
    };

    class KuddMtpDialogsType : public KuddMtpBaseType {
    public:
        KuddMtpDialogsType();

    public:
        virtual void write(KuddSequentialMtpRepository* repository);

    public:
        void setDialogs(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository);

    private:
        quint32 count_;
        QVector<KuddMtpDialogType> dialogs_;
    };


    class KuddMtpDialogMessageType : public KuddMtpBaseType {
    public:
        KuddMtpDialogMessageType();

    public:
        virtual void write(KuddSequentialMtpRepository* repository);

    public:
        void convert(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository);

    private:
        KuddMtpDialogsType dialogs_;
        KuddMtpArticlesType articles_;
        KuddMtpChatsType chats_;
        KuddMtpUsersType users_;
    };

    class KuddSequentialMtpRepository {
    public:
        void reposit(const void* value);  // only address
        void reposit(qint8 value);
        void reposit(qint32 value);
        void reposit(quint32 value);
        void reposit(const MTPInt& value);
        void reposit(const MTPBool& value);
        void reposit(const QString& value);

        const mtpPrime* toMtpPrime();

        qint32 size() const;

    private:
        QByteArray repository_;
    };

    
}

#endif //_KUDD_DIGAL_FLASH_TYPES_H_
