#include "stdafx.h"
#include "KuddDigalFlashTypes.h"
#include "KuddModuleConst.h"
#include "history.h"
#include <cstdint>
#include "../mtproto/mtp.h"

using namespace kudd;

namespace {
    const PeerId PEERID_DIGAL_FLASH_MANAGER = UINT64_MAX;
}

KuddMtpBaseType::operator mtpTypeId()
{
    return typeId_;
}

void KuddMtpBaseType::write(KuddSequentialMtpRepository* repository)
{
    repository->reposit(typeId_);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpArticleType::KuddMtpArticleType()
    : KuddMtpBaseType(mtpc_message)  // FIXME
{
    static int32 s_id = 0;

    // FIXME
    vflags_.v = 0;
    vid_.v = s_id++;
    if (s_id < 0) {
        s_id = 0;
    }
    vfrom_id_.v = PEER_ID_KUDD_MODULE;
    vto_id_.v = MTP::authedId();// PEER_ID_KUDD_MODULE; // 231583901;  // TODO : Get My ID
    
    QDateTime now = QDateTime::currentDateTimeUtc();
    vdate_.v = static_cast<qint32>(now.toTime_t());

    //vmessage_ = "this is a test   ";// "\xec\x9e\x84";// "\xec\x9d\xb4\xea\xb2\x83\xec\x9d\x80 \xed\x85\x8c\xec\x8a\xa4\xed\x8a\xb8 \xeb\xa9\x94\xec\x8b\x9c\xec\xa7\x80\xec\x9e\x85\xeb\x8b\x88\xeb\x8b\xa4."; //"이것은 테스트 메시지입니다.";
    //vmessage_ = "\xec\x9d\xb4\xea\xb2\x83\xec\x9d\x80 \xed\x85\x8c\xec\x8a\xa4\xed\x8a\xb8 \xeb\xa9\x94\xec\x8b\x9c\xec\xa7\x80\xec\x9e\x85\xeb\x8b\x88\xeb\x8b\xa4."; //"이것은 테스트 메시지입니다.";
}

KuddMtpArticleType::KuddMtpArticleType(const KuddNaverCafeArticle& article)
    : KuddMtpArticleType()  // delegating constructor (c++11)
{
    vmessage_ = QString("%1 by %2\r\n%3").arg(article.title(), article.author(), article.link());
}

void KuddMtpArticleType::write(KuddSequentialMtpRepository* repository)
{
    MTPPeer peper;
    KuddMtpBaseType::write(repository);
    repository->reposit(vflags_);
    repository->reposit(vid_);
    repository->reposit(vfrom_id_);

    // MTPPeer
    repository->reposit(mtpc_peerUser);
    repository->reposit(vto_id_);
    // ..MTPPeer

    repository->reposit(vdate_);

    // MTPstring
    // DO NOT NEED mtpTypeId
    //repository->reposit(mtpc_string);
    repository->reposit(vmessage_);
    // .. MTPstring

    // MTPMessageMedia
    repository->reposit(mtpc_messageMediaEmpty);
    // ..MTPMessageMedia


}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpChatType::KuddMtpChatType()
    : KuddMtpBaseType(mtpc_chat)
{
    static qint32 s_id = 0;
    vid_.v = s_id++;
    if (s_id < 0) {
        s_id = 0;
    }

    vtitle_ = "Kudd MTP Module Chat Unit";
    // vphoto_ type = mtpc_chatPhotoEmpty;
    vparticipants_count_.v = 2; // me and the module

    QDateTime now = QDateTime::currentDateTimeUtc();
    //QDate today = QDate::currentDate();
    vdate_.v = static_cast<qint32>(now.toTime_t());
    vleft_ = false;
    vversion_.v = 0; // FIXME
}

KuddMtpChatType::KuddMtpChatType(const KuddNaverCafeArticle& article)
    : KuddMtpChatType() // delegating constructor
{
    // FIXME : format..
    vtitle_ = article.title();
}

void KuddMtpChatType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);

    repository->reposit(vid_);
    repository->reposit(vtitle_);
    repository->reposit(mtpc_chatPhotoEmpty);
    repository->reposit(vparticipants_count_);
    repository->reposit(vdate_);
    repository->reposit(vleft_ ? mtpc_boolTrue : mtpc_boolFalse);
    repository->reposit(vversion_);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpChatsType::KuddMtpChatsType()
    : KuddMtpBaseType(mtpc_vector)
    , count_(0)
{
}

void KuddMtpChatsType::setChats(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository)
{
    for (const auto& each : articles) {
        chats_.push_back(*each);
    }
    count_ = static_cast<quint32>(chats_.size());
}

void KuddMtpChatsType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);
    repository->reposit(count_);

    for (auto& each : chats_) {
        each.write(repository);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpUsersType::KuddMtpUsersType()
    : KuddMtpBaseType(mtpc_vector)
    , count_(0)
{
}

void KuddMtpUsersType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);
    repository->reposit(count_);

    // TODO: write actual users..
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpArticlesType::KuddMtpArticlesType()
    : KuddMtpBaseType(mtpc_vector)
    , count_(0)
{
}

KuddMtpArticlesType::~KuddMtpArticlesType()
{
}

void KuddMtpArticlesType::setArticles(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository)
{
    quint32 numChanges = 0;
    for (const auto& each : articles) {
        if (each->isNew()) {  // TODO: FIXME: add OR condition.
            ++numChanges;
            articles_.push_back(*each);
        }
    }
    count_ = numChanges;
}

qint32 KuddMtpArticlesType::lastArticleId() const
{
    return articles_.empty() ? -1 : articles_.constEnd()->vId();
}

void KuddMtpArticlesType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);
    repository->reposit(count_);

    for (auto& each : articles_) {
        each.write(repository);
    }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpArticleMessageType::KuddMtpArticleMessageType()
    : KuddMtpBaseType(mtpc_messages_messages)
{
    
}

void KuddMtpArticleMessageType::convert(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository)
{
    articles_.setArticles(articles, repository);
    chats_.setChats(articles, repository);
    // zero users
}

void KuddMtpArticleMessageType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);
    articles_.write(repository);
    chats_.write(repository);
    users_.write(repository);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpPeerNotifySettings::KuddMtpPeerNotifySettings()
    : KuddMtpBaseType(mtpc_peerNotifySettingsEmpty)
{
}

void KuddMtpPeerNotifySettings::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);

    // we don't need to serialize another fields because mtpTypeId is mtpc_peerNotifySettingsEmpty..
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpDialogType::KuddMtpDialogType()
    : KuddMtpBaseType(mtpc_dialog)
{
    static qint32 s_virtual_message_id = 200000000;
    vpeer_.v = PEER_ID_KUDD_MODULE;
    vtop_message_.v = s_virtual_message_id++;    // FIXME. requires messageId ??
    vtop_unread_count_.v = 0;
}

void KuddMtpDialogType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);

    repository->reposit(mtpc_peerUser);
    repository->reposit(vpeer_);
    repository->reposit(vtop_message_);
    repository->reposit(vtop_unread_count_);

    vnotify_settings_.write(repository);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpDialogsType::KuddMtpDialogsType()
    : KuddMtpBaseType(mtpc_vector)
    , count_(0)
{
}

void KuddMtpDialogsType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);

    if (!dialogs_.empty()) {

        // count should be 1
        repository->reposit(count_);

        dialogs_[0].write(repository);
        //
        //for (auto& each : dialogs_) {
        //    each.write(repository);
        //}
    }
}

namespace {
    struct Counter {
        Counter(bool title, bool comment)
            : _title(title)
            , _comment(comment)
        {}

        bool _title;
        bool _comment;

        bool operator()(const KuddNaverCafeArticleSp& each) const
        {
            int32 count = 0;
            if (_title && each->isNew()) {
                ++count;
            }
            if (_comment && each->commentNumChanged()) {
                ++count;
            }
            return count != 0;
        }
    };
}

void KuddMtpDialogsType::setDialogs(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository)
{
    KuddMtpDialogType dlg;

    //dlg.setTopMessage()
    // FIXME : unreadCount.. to number of changed articles...

    count_ = static_cast<int32>(std::count_if(articles.begin(), articles.end(), Counter(true, false)));

    const auto where = std::find_if(articles.begin(), articles.end(), Counter(true, false));
    if (where != articles.end()) {
        const auto& title = (*where)->title();
        qDebug() << "title = " << title;
    }

    dlg.setUnreadCount(count_);

    dialogs_.push_back(dlg);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddMtpDialogMessageType::KuddMtpDialogMessageType()
    : KuddMtpBaseType(mtpc_messages_dialogs)
{
}

void KuddMtpDialogMessageType::write(KuddSequentialMtpRepository* repository)
{
    KuddMtpBaseType::write(repository);

    dialogs_.write(repository);
    articles_.write(repository);
    chats_.write(repository);
    users_.write(repository);
}

void KuddMtpDialogMessageType::convert(const QVector<KuddNaverCafeArticleSp>& articles, KuddSequentialMtpRepository* repository)
{
    dialogs_.setDialogs(articles, repository);
    articles_.setArticles(articles, repository);
    chats_.setChats(articles, repository);
    // zero users
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddNaverCafeArticle::KuddNaverCafeArticle() 
    : type_(FLASH)
    , new_(false)
    , commentNumChanged_(false)
    , numComments_(0)
    , numPreviousComments_(0)
{
}

bool KuddNaverCafeArticle::operator==(const KuddNaverCafeArticle& r) const
{
    return articleId_ == r.articleId_;
}

bool KuddNaverCafeArticle::operator!=(const KuddNaverCafeArticle& r) const
{
    return !operator==(r);
}


//////////////////////////////////////////////////////////////////////////////////////////////////////////////

KuddNaverCafeArticles::KuddNaverCafeArticles(History* history, HistoryBlock* block, MsgId msgId, bool out, bool unread, QDateTime msgDate, int32 from)
    : HistoryItem(history, block, msgId, out, unread, msgDate, from)
{
    makeHistory();
}

// FIXME: from
KuddNaverCafeArticles::KuddNaverCafeArticles()
    : HistoryItem(nullptr, nullptr, 0, false, false, QDateTime::currentDateTime(), -1)
{
    makeHistory();
}

void KuddNaverCafeArticles::makeHistory()
{
    _history = App::history(PEERID_DIGAL_FLASH_MANAGER);
}

void KuddNaverCafeArticles::setArticleType(quint8 type)
{
    for (auto& each : articles_) {
        each->setType(type);
    }
}

void KuddNaverCafeArticles::copyArticles(const KuddNaverCafeArticles& articles)
{
    //std::copy(articles.articles().begin(), articles.articles().end(), std::back_inserter(articles_));
    for (const auto& each : articles.articles()) {
        if (articleTitles_.find(each->title()) == articleTitles_.end()) {
            articleTitles_.insert(each->title());
            articles_.push_back(each);
        }
    }
}

// implementation of HistoryItem
void KuddNaverCafeArticles::initDimensions(const HistoryItem *parent)
{
}

int32 KuddNaverCafeArticles::resize(int32 width, bool dontRecountText, const HistoryItem *parent)
{
    return 100;
}

void KuddNaverCafeArticles::draw(QPainter &p, uint32 selection) const
{
}

void KuddNaverCafeArticles::drawInDialog(QPainter &p, const QRect &r, bool act, const HistoryItem *&cacheFor, Text &cache) const
{
}

QString KuddNaverCafeArticles::notificationText() const
{
    return "";
}

void KuddNaverCafeArticles::convertToMtpMessages(KuddMtpArticleMessageType* articles, KuddSequentialMtpRepository* repository) const
{
    articles->convert(articles_, repository);
    articles->write(repository);
}

void KuddNaverCafeArticles::convertToMtpDialogs(KuddMtpDialogMessageType* dialogs, KuddSequentialMtpRepository* repository) const
{
    dialogs->convert(articles_, repository);
    dialogs->write(repository);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////

void KuddSequentialMtpRepository::reposit(const void* value)
{
    repository_.append((const char*)&value, sizeof(void*));
}

void KuddSequentialMtpRepository::reposit(qint8 value)
{
    repository_.append((const char*)&value, sizeof(value));
}

void KuddSequentialMtpRepository::reposit(qint32 value)
{
    repository_.append((const char*)&value, sizeof(value));
}

void KuddSequentialMtpRepository::reposit(quint32 value)
{
    repository_.append((const char*)&value, sizeof(value));
}

void KuddSequentialMtpRepository::reposit(const MTPInt& value)
{
    // MTPInt does not writes mtpType
    //reposit(value.type());
    reposit(value.v);
}

void KuddSequentialMtpRepository::reposit(const MTPBool& value)
{
    // MTPBool does not writes mtpType
    //reposit(value.type());
    repository_.append((const char*)(&value.v), sizeof(value.v));
}

namespace {
    quint32 makeStringLength(quint32 length)
    {
#if !defined(_WIN32)
#error "TEST BIG ENDIANs"
#endif
        if (length > 0x00ffffff) {
            throw "out of range";
        }

        quint32 v = length << 8;
        v |= 0x000000fe;        // FE = identifier

        return v;
    }
}

void KuddSequentialMtpRepository::reposit(const QString& value)
{
    QByteArray data = value.toUtf8();
    const char* data_ptr = data.data();
    reposit(static_cast<quint32>(makeStringLength(data.size())));
    repository_.append(data_ptr, data.size());

    qint32 padding = 4 - (data.size() % 4);
    if (padding != 4) {
        for (qint32 i = 0; i < padding; ++i) {
            reposit(static_cast<qint8>(0));
        }
    }

    //const ushort* data = value.utf16();
    //repository_.append((const char*)data, sizeof(ushort)*value.size());
        
}

const mtpPrime* KuddSequentialMtpRepository::toMtpPrime()
{
    const void* vptr = repository_.data_ptr()->data();
    return reinterpret_cast<const mtpPrime*>(vptr);
}

qint32 KuddSequentialMtpRepository::size() const
{
    return repository_.size();
}
