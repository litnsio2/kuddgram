#ifndef _KUDD_DIGAL_FLASH_MANAGER_H_
#define _KUDD_DIGAL_FLASH_MANAGER_H_

#include <QtCore/QSharedPointer>
#include <QtCore/QMutex>
#include "KuddDigalFlashTypes.h"

namespace kudd {
    class NaverCafeBoardDownloader;
    class KuddModuleManager;

    class KuddDigalFlashManager : public QObject {
        Q_OBJECT;

    public:
        KuddDigalFlashManager(KuddModuleManager* owner);

    public:
        void check();

    private:
        void checkForNewArticles();

    private:
        QSharedPointer<NaverCafeBoardDownloader> downloader_;
        KuddNaverCafeArticles lastArticles_;
        KuddNaverCafeArticles currentArticles_;

        QMutex mutex_;
        bool working_;

        KuddModuleManager* owner_;

    private slots:
        void boardDownloaded(quint8 boardType, const KuddNaverCafeArticles& articles);
        void boardDownloadError(QNetworkReply::NetworkError error);
    };
}

#endif //_KUDD_DIGAL_FLASH_MANAGER_H_
