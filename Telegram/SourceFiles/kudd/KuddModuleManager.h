#ifndef _KUDD_MODULE_MANAGER_H_
#define _KUDD_MODULE_MANAGER_H_

#include <QtCore/QSharedPointer>

class QTimer;

namespace kudd {
    class KuddDigalFlashManager;
    class KuddNaverCafeArticles;

    class KuddModuleManager : public QObject {
        Q_OBJECT;
        friend class KuddDigalFlashManager;

    public:
        KuddModuleManager();
        void start();
        void finish();

    private slots:
        void onNewArticleDetected(const QString& cafeName, const KuddNaverCafeArticles& articles);
        void onTimerExpired();

    private:
        QSharedPointer<KuddDigalFlashManager> dfm_;
        QSharedPointer<QTimer> timer_;

    signals:
        void naverCafeArticleDetected(const QString&, const kudd::KuddNaverCafeArticles&);
    };
}

#endif //_KUDD_MODULE_MANAGER_H_
