#ifndef _KUDD_MODULE_CONST_H_
#define _KUDD_MODULE_CONST_H_

#include <QtCore/QString>
#include <QtCore/QVector>
#include <QtCore/QSet>
#include <QtCore/QSharedPointer>
#include "../history.h"
#include <cstdint>

namespace kudd {
    static const qint64  PEER_ID_KUDD_MODULE   = INT32_MAX;
    static const qint32  MSGID_KUDD_MODULE     = INT32_MAX;
    //static const qint32  REQUESTID_KUDD_MODULE = INT32_MAX;
}

#endif //_KUDD_MODULE_CONST_H_
