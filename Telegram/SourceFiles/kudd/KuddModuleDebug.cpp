#include "stdafx.h"
#include "KuddModuleDebug.h"
#include "../history.h"
#include <QtCore/QDebug>

using namespace kudd;

KuddDebug& KuddDebug::get()
{
    static KuddDebug __instance__;
    return __instance__;
}

namespace {
    QString makeQString(const MTPstring& s) {
        return QString::fromUtf8(s.c_string().v.c_str());
    }

    void printMTPDpeerUser(const MTPDpeerUser& user) {
        qDebug() << "  - peerType : peerUser, peerId : " << user.vuser_id.v;
    }
    
    void printMTPChats(const MTPVector<MTPChat>& chats) {
        qDebug() << " * chats";

        const auto& chats_vector = chats.c_vector();
        int32 count = 0;
        for (const auto& each : chats_vector.v) {
            if (each.type() == mtpc_chat) {
                qDebug() << "[" << count ++ << "]  - chat : mtpcChat";
                const auto& chat = each.c_chat();

                QDateTime dt = QDateTime::fromTime_t(chat.vdate.v);
                qDebug() << "   : date " << dt.toString();
            }
            else {
                qDebug() << "  - chatType : " << each.type();
            }
        }
    }

    void printMTPMessages(const MTPVector<MTPMessage>& messsages) {
        qDebug() << " * messages";

        const auto& messages_vector = messsages.c_vector();
        int32 count = 0;
        for (const auto& each : messages_vector.v) {
            if (each.type() == mtpc_message) {
                qDebug() << "[" << count++ << "]  - message : message";
                const auto& message = each.c_message();
                
                qDebug() << "  : message " << makeQString(message.vmessage);
                qDebug() << "  : date " << QDateTime::fromTime_t(message.vdate.v).toString();
                qDebug() << "  : from_id " << message.vfrom_id.v;
            }
            else {
                qDebug() << "  - messageType : " << each.type();
            }
        }
    }

    void printMTPUsers(const MTPVector<MTPUser>& users) {
        qDebug() << " * users";

        const auto& users_vector = users.c_vector();
        int32 count = 0;
        for (const auto& each : users_vector.v) {
            if (each.type() == mtpc_userContact) {
                qDebug() << "[" << count++ << "]  - user : userContact";

                const auto& user = each.c_userContact();
                qDebug() << "  : access_hash " << user.vaccess_hash.v;
                qDebug() << "  : last_name " << makeQString(user.vlast_name);
                qDebug() << "  : first_name " << makeQString(user.vfirst_name);
                qDebug() << "  : user_name " << makeQString(user.vusername);
            }
            else {

                qDebug() << "  - userType : " << each.type();
            }
        }
    }
}


void KuddDebug::printMessageDialogs(const MTPmessages_Dialogs& d)
{
    if (d.type() == mtpc_messages_dialogs) {
        const auto& dialogs = d.c_messages_dialogs();
        const auto& dialogs_internal = dialogs.vdialogs;
        const auto& dialogs_internal_vector = dialogs_internal.c_vector();

        int32 count = 0;
        qDebug() << "printing MTPmessages_Dialogs";
        qDebug() << "* dialogs";
        for (const auto& each : dialogs_internal_vector.v) {
            const auto& raw_dialog = each.c_dialog();
            qDebug() << "[" << count++ << "]";
            if (raw_dialog.vpeer.type() == mtpc_peerUser) {
                printMTPDpeerUser(raw_dialog.vpeer.c_peerUser());
            }
            else {
                qDebug() << "  - peerType : peerChat";
            }
        }

        printMTPUsers(dialogs.vusers);
        printMTPChats(dialogs.vchats);
        printMTPMessages(dialogs.vmessages);
    }
}

void KuddDebug::printDialogsList(DialogsList* lst)
{
    qDebug() << "Print DialogsList, this = " << lst;
    DialogRow* c = lst->begin;

    for (qint32 count = 0; c != lst->end; c = c->next, ++count) {
        qDebug() << count << " : " << c->history->peer->name;
    }
}

void KuddDebug::printString(const QString& str)
{
    qDebug() << str;
}

void KuddDebug::printString(const QString& prefix, const QString& str)
{
    qDebug() << prefix << " : " << str;
}

