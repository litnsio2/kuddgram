#ifndef _KUDD_DIGAL_FLASH_PARSER_H_
#define _KUDD_DIGAL_FLASH_PARSER_H_

namespace kudd {
    class KuddNaverCafeArticles;
    class KuddDigalFlashParser {
    public:
        static bool parseArticles(const char* text, const QString& cafeName, KuddNaverCafeArticles* articles);
    };
}

#endif //_KUDD_DIGAL_FLASH_PARSER_H_
