#include "stdafx.h"
#include "KuddModuleManager.h"
#include "KuddDigalFlashManager.h"
#include <QtCore/QTimer>

#if defined(_WIN32)
    //#include <fileapi.h>
    #include <DbgHelp.h>
    #pragma comment(lib, "dbghelp.lib")
#endif

using namespace kudd;

namespace {
    class Sleeper : public QThread {
    public:
        static void msleep(quint32 msecs) { QThread::msleep(msecs); }
    };

#if defined(_WIN32)
    LONG WINAPI Kudd_UnhandledExceptionHandler(struct _EXCEPTION_POINTERS* exceptionInfo)
    {
        QDate today = QDateTime::currentDateTime().date();
        QTime now = QTime::currentTime();
        
        wchar_t fmt[256] = { 0, };
        wsprintf(fmt, L"KuddGram_%04d%02d%02d-%02d%02d%02d.dmp", today.year(), today.month(), today.day(), now.hour(), now.minute(), now.second());

        HANDLE hFile = CreateFileW(fmt,
            GENERIC_WRITE,
            0,
            NULL,
            CREATE_ALWAYS,
            FILE_ATTRIBUTE_NORMAL,
            NULL);

        MINIDUMP_EXCEPTION_INFORMATION aMiniDumpInfo;
        aMiniDumpInfo.ThreadId = GetCurrentThreadId();
        aMiniDumpInfo.ExceptionPointers = exceptionInfo;
        aMiniDumpInfo.ClientPointers = TRUE;

        MiniDumpWriteDump(GetCurrentProcess(),
            GetCurrentProcessId(),
            hFile,
            (MINIDUMP_TYPE)(MiniDumpWithFullMemory | MiniDumpWithHandleData),
            &aMiniDumpInfo,
            NULL,
            NULL);

        CloseHandle(hFile);

        return EXCEPTION_EXECUTE_HANDLER;
    }
#endif

    void setUnhandledExceptionFilter()
    {
#if defined(_WIN32)
        SetUnhandledExceptionFilter(&Kudd_UnhandledExceptionHandler);
#endif
    }
}

KuddModuleManager::KuddModuleManager()
{
    dfm_.reset(new KuddDigalFlashManager(this));
    timer_.reset(new QTimer(this));

    connect(timer_.data(), SIGNAL(timeout()), this, SLOT(onTimerExpired()));

    setUnhandledExceptionFilter();
}

void KuddModuleManager::start()
{
    timer_->start(1000 * 10);
}

void KuddModuleManager::finish()
{
    timer_->stop();
}

void KuddModuleManager::onTimerExpired()
{
    dfm_->check();

}

void KuddModuleManager::onNewArticleDetected(const QString& cafeName, const KuddNaverCafeArticles& articles)
{
    emit naverCafeArticleDetected(cafeName, articles);
}
