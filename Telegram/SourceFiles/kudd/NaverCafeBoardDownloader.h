#ifndef _NAVER_CAFE_BOARD_DOWNLOADER_H_
#define _NAVER_CAFE_BOARD_DOWNLOADER_H_

#include <QtCore/QString>
#include <QtCore/QUrl>
#include <QtNetwork/QNetworkAccessManager>
#include "KuddDigalFlashTypes.h"

class QNetworkReply;

namespace kudd {
    class KuddModuleManager;
    class KuddNaverCafeArticles;
    class NaverCafeBoardDownloader : public QObject {
        Q_OBJECT;

    public:
        NaverCafeBoardDownloader();

    public:
        bool download(quint8 boardType, const QString& uri, const QString& cafeName);
        bool parse();

    private:
        QNetworkAccessManager nam_;
        QUrl url_;
        QNetworkReply* reply_;
        QByteArray lastResponse_;
        quint8 currentBoardType_;
        QString cafeName_;

    signals:
        void boardDownloaded(quint8, const KuddNaverCafeArticles&);
        void boardDownloadError(QNetworkReply::NetworkError);

    private slots:
        void httpFinished();
        void httpReadyRead();
        void updateDataReadProgress(quint64, quint64);
        void onReplyError(QNetworkReply::NetworkError);
    };
}

#endif //_NAVER_CAFE_BOARD_DOWNLOADER_H_
