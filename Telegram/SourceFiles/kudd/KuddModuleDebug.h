#ifndef _KUDD_MODULE_DEBUG_H_
#define _KUDD_MODULE_DEBUG_H_
#include "../mtproto/mtpScheme.h"

struct DialogsList;
namespace kudd {
    class KuddDebug {
    public:
        static KuddDebug& get();
        ~KuddDebug() {}

    public:
        void printMessageDialogs(const MTPmessages_Dialogs& d);
        void printDialogsList(DialogsList* lst);
        void printString(const QString& str);
        void printString(const QString& prefix, const QString& str);

    private:
        KuddDebug() {}
    };
}

#endif //_KUDD_MODULE_DEBUG_H_
