#include "stdafx.h"
#include "KuddDigalFlashParser.h"
#include "KuddDigalFlashTypes.h"
#include <QtCore/QMap>
#include <tidy.h>
#include <buffio.h>

using namespace kudd;

namespace {
    TidyBool TIDY_CALL reportFilter(TidyDoc doc, TidyReportLevel level, uint line, uint col, ctmbstr msg)
    {
        return no;
    }

    typedef QMap<QString, QString> AttrMap;
    bool __hasAttr(const AttrMap& attrMap, const QString& attr, const QString& value)
    {
        return attrMap.find(attr) != attrMap.end() && attrMap[attr] == value;
    }

    QString __extractIdFromLink(const QString& link)
    {
        // e.g.) /ArticleRead.nhn?clubid=10115278&menuid=370&articleid=548480&page=1&boardtype=L
        qint32 begin = link.indexOf("articleid=");
        if (begin >= 0) {
            qint32 end = link.indexOf("&", begin + 10);
            qint32 count = end - begin - 10;
            return link.mid(begin + 10, count);
        }
        return "";
    }

    void __getAttrMap(TidyNode node, AttrMap& attrMap)
    {
        for (TidyAttr attr = tidyAttrFirst(node); attr; attr = tidyAttrNext(attr)) {
            const char* cstrnm = tidyAttrName(attr);
            const char* cstrval = tidyAttrValue(attr);

            if (cstrnm && cstrval) {
                attrMap.insert(cstrnm, cstrval);
            }
        }
    }

    void __parseNoticeList(TidyDoc doc, TidyNode node, const QString& cafeName, KuddNaverCafeArticles* articles, bool skipDiv)
    {
        qint32 count = 0;
        for (TidyNode li = tidyGetChild(node); li; li = tidyGetNext(li)) {
            ++count;

            KuddNaverCafeArticleSp article(new KuddNaverCafeArticle());

            QString title, author, date, comment;

            TidyNode aLink = tidyGetChild(li);  // has article link
            AttrMap aLinkAttrs;
            __getAttrMap(aLink, aLinkAttrs);

            TidyNode pNode = 0;
            pNode = tidyGetNext(aLink);

            QString pNodeName = tidyNodeGetName(pNode);
            TidyNode strongNode = 0;
            if (skipDiv && pNodeName != "p") {
                pNode = tidyGetNext(pNode);
            }

            strongNode = tidyGetChild(pNode);
            TidyNode strongTextNode = 0;
            TidyNode brNode = 0;

            if (skipDiv) {
                strongTextNode = tidyGetChild(strongNode);
            }
            else {
                TidyNode spanNode = tidyGetChild(strongNode);
                strongTextNode = tidyGetNext(spanNode);
            }

            TidyBuffer buffer;
            memset(&buffer, 0, sizeof(buffer));

            tidyNodeGetValue(doc, strongTextNode, &buffer);
            title = QString::fromUtf8(reinterpret_cast<char*>(buffer.bp));
            
            brNode = tidyGetNext(strongNode);
            QString brNodeName = tidyNodeGetName(brNode);
            while (brNodeName != "br") {
                // in case of  "<span class="ic_new">New</span>" current node is "New" (text node)
                if (tidyNodeGetType(brNode) == TidyNode_Text) {
                    brNode = tidyGetNext(brNode); // skip it
                }
                AttrMap forDebug;
                __getAttrMap(brNode, forDebug);
                brNode = tidyGetNext(brNode);
                brNodeName = tidyNodeGetName(brNode);
            }

            TidyNode spanInfoNode = tidyGetNext(brNode);
            TidyNode spanTyNode = tidyGetChild(spanInfoNode);
            TidyNode spanTyNodeText = tidyGetChild(spanTyNode);
            TidyNode spanBarNode1 = tidyGetNext(spanTyNode);
            TidyNode spanBarNodeText1 = tidyGetNext(spanBarNode1);
            spanBarNodeText1 = tidyGetNext(spanBarNodeText1);
            TidyNode spanBarNode2 = tidyGetNext(spanBarNodeText1);
            TidyNode spanBarNodeText2 = tidyGetNext(spanBarNode2);

            TidyNode aCmtLink = tidyGetNext(pNode);
            TidyNode spanHcNode = tidyGetChild(aCmtLink);
            TidyNode aCmtLinkText = tidyGetNext(spanHcNode);

            tidyNodeGetValue(doc, aCmtLinkText, &buffer);
            comment = QString::fromUtf8(reinterpret_cast<char*>(buffer.bp));
                        
            tidyNodeGetValue(doc, spanTyNodeText, &buffer);
            author = QString::fromUtf8(reinterpret_cast<char*>(buffer.bp));

            tidyNodeGetValue(doc, spanBarNodeText1, &buffer);
            date = QString::fromUtf8(reinterpret_cast<char*>(buffer.bp));

            article->setTitle(title);
            article->setDate(date);
            article->setAuthor(author);
            article->setArticleId(__extractIdFromLink(aLinkAttrs["href"]));
            article->setNumComments(comment.toUShort());
            article->setLink(QString("http://cafe.naver.com/%1/%2").arg(cafeName, article->articleId()));

            articles->addArticle(article);
        }
    }
    
    void __parseArticle(TidyDoc doc, TidyNode node, const QString& cafeName, KuddNaverCafeArticles* articles)
    {
        // first, find node <ul with id=noticeList
        const char* nodeNameCstr = tidyNodeGetName(node);

        AttrMap attrMap;
        QString nodeName = nodeNameCstr;
        if (nodeName == "ul") {
            __getAttrMap(node, attrMap);

            if (__hasAttr(attrMap, "class", "lst4")) {
                if (__hasAttr(attrMap, "id", "noticeList")) {
                    __parseNoticeList(doc, node, cafeName, articles, false);
                }
                else {
                    __parseNoticeList(doc, node, cafeName, articles, true);
                }
            }
        }
        else {
            for (TidyNode child = tidyGetChild(node); child; child = tidyGetNext(child)) {
                __parseArticle(doc, child, cafeName, articles);
            }
        }
    }
}

bool KuddDigalFlashParser::parseArticles(const char* text, const QString& cafeName, KuddNaverCafeArticles* articles)
{
    bool ret = true;

    try {
        TidyDoc tdoc = tidyCreate();

        tidySetReportFilter(tdoc, reportFilter);
        tidySetInCharEncoding(tdoc, "utf8");
        tidyParseString(tdoc, text);

        TidyNode root = tidyGetRoot(tdoc);
        __parseArticle(tdoc, root, cafeName, articles);

        tidyRelease(tdoc);
    }
    catch (...) {
        ret = false;
    }

    return ret;
}
