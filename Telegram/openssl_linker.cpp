#include "stdafx.h"

#if defined(_DEBUG)
#pragma comment(lib, "../../KuddGramLibs/openssl/msvc12.0/libeay32_mtd.lib")
#pragma comment(lib, "../../KuddGramLibs/openssl/msvc12.0/ssleay32_mtd.lib")
#else
#pragma comment(lib, "../../KuddGramLibs/openssl/msvc12.0/libeay32_mt.lib")
#pragma comment(lib, "../../KuddGramLibs/openssl/msvc12.0/ssleay32_mt.lib")
#endif

